package api

import (
	"os"
	"strconv"

	"github.com/joho/godotenv"

	"gitlab.com/projectstolaunch/assignmentportal/backend/api/controllers"
	"gitlab.com/projectstolaunch/assignmentportal/backend/logger"
)

var server = controllers.Server{}

// Run is starting point for the server
func Run() {
	var err error

	err = godotenv.Load()
	if err != nil {
		logger.Fatal(1, "Failed to load environment variables")
	} else {
		logger.Println(1, "Environment Variables Loaded")
	}

	debugMode, err := strconv.Atoi(os.Getenv("APP_DEBUG_MODE"))
	if err != nil {
		logger.Fatal(1, "Failed to load environment variables")
	}

	server.Initialize(debugMode, os.Getenv("DB_DRIVER"), os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_PORT"), os.Getenv("DB_HOST"), os.Getenv("DB_NAME"), 5)

	server.Run("0.0.0.0:4000")
}
