package models

import (
	"context"
	"errors"
	"time"

	"gitlab.com/projectstolaunch/assignmentportal/backend/logger"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// User is struct to describe course objects
type User struct {
	ID        primitive.ObjectID `bson:"_id" json:"id,omitempty" query:"id"`
	CreatedAt time.Time          `bson:"created_at" json:"created_at" query:"created_at"`
	UpdatedAt time.Time          `bson:"updated_at" json:"updated_at" query:"updated_at"`

	Nickname         string `bson:"user_display_name" json:"user_display_name" form:"user_display_name" query:"user_display_name"`
	Email            string `bson:"user_email" json:"user_email" form:"user_email" query:"user_email"`
	Phone            string `bson:"user_phone" json:"user_phone" form:"user_phone" query:"user_phone"`
	Password         string `bson:"password" form:"password"`
	TutorDescription string `bson:"tutor_description" json:"tutor_description" form:"tutor_description" query:"tutor_description"`

	Views int64 `bson:"views" json:"views" form:"views" query:"views"`

	IsAdmin         bool         `bson:"is_admin" json:"is_admin" form:"is_admin" query:"is_admin"`
	IsTutor         bool         `bson:"is_tutor" json:"is_tutor" form:"is_tutor" query:"is_tutor"`
	IsVerified      bool         `bson:"is_verified" json:"is_verified" form:"is_verified" query:"is_verified"`
	SolutionUploads []Assignment `bson:"solution_uploads" json:"solution_uploads" form:"solution_uploads" query:"solution_uploads"`
}

var userCollName = "User"

// Validate performs minimal validation
func (obj *User) Validate() error {
	if len(obj.Nickname) < 1 {
		return errors.New("Nickname should be atleast 5 characters")
	}

	if len(obj.Email) < 1 {
		return errors.New("Email cannot be empty")
	}

	if len(obj.Password) < 1 {
		return errors.New("Password should be minimum 1 characters")
	}

	return nil
}

// SaveAdmin saves the object in database
func (obj *User) SaveAdmin(debugMode int, ctx context.Context, database *mongo.Database) (*User, error) {
	var err error

	err = database.Collection(userCollName).FindOne(ctx, bson.M{"user_email": obj.Email}).Decode(&obj)
	if err != nil {
		logger.Println(debugMode, err.Error())
	} else {
		return nil, errors.New("User with that email already registered")
	}
	obj.ID = primitive.NewObjectID()
	obj.CreatedAt = time.Now()
	obj.UpdatedAt = time.Now()
	var tmpSolArr []Assignment
	obj.SolutionUploads = tmpSolArr

	_, err = database.Collection(userCollName).InsertOne(ctx, obj)
	if err != nil {
		return nil, err
	}

	return obj, nil
}

// Save saves the object in database
func (obj *User) Save(debugMode int, ctx context.Context, database *mongo.Database) (*User, error) {
	var err error

	err = database.Collection(userCollName).FindOne(ctx, bson.M{"user_email": obj.Email}).Decode(&obj)
	if err != nil {
		logger.Println(debugMode, err.Error())
	} else {
		return nil, errors.New("User with that email already registered")
	}

	// var result *mongo.InsertOneResult

	// result, err = database.Collection(assignmentCollName).InsertOne(ctx, obj)
	obj.ID = primitive.NewObjectID()
	obj.CreatedAt = time.Now()
	obj.UpdatedAt = time.Now()
	obj.IsAdmin = false
	obj.IsTutor = false
	obj.IsVerified = false
	var tmpSolArr []Assignment
	obj.SolutionUploads = tmpSolArr

	_, err = database.Collection(userCollName).InsertOne(ctx, obj)
	if err != nil {
		return nil, err
	}

	return obj, nil
}

// UpdatePassword saves the object in database
func (obj *User) UpdatePassword(ctx context.Context, database *mongo.Database) (*User, error) {
	var err error

	filter := bson.M{"_id": obj.ID}

	update := bson.D{{"$set", bson.D{
		{"password", obj.Password},
	}}}

	_, err = database.Collection(userCollName).UpdateOne(ctx, filter, update)
	if err != nil {
		return nil, err
	}

	return obj, nil
}

// UpgradeToTutor saves the object in database
func (obj *User) UpgradeToTutor(ctx context.Context, database *mongo.Database) (*User, error) {
	var err error

	filter := bson.M{"_id": obj.ID}

	update := bson.D{{"$set", bson.D{
		{"is_tutor", true},
	}}}

	_, err = database.Collection(userCollName).UpdateOne(ctx, filter, update)
	if err != nil {
		return nil, err
	}

	return obj, nil
}

// Update saves the object in database
func (obj *User) Update(ctx context.Context, database *mongo.Database) (*User, error) {
	var err error

	filter := bson.M{"_id": obj.ID}

	update := bson.D{{"$set", bson.D{
		{"updated_at", time.Now()},
		{"user_display_name", obj.Nickname},
		{"user_phone", obj.Phone},
		{"tutor_description", obj.TutorDescription},
	}}}

	_, err = database.Collection(userCollName).UpdateOne(ctx, filter, update)
	if err != nil {
		return nil, err
	}

	return obj, nil
}

// FindByEmail by email
func (obj *User) FindByEmail(ctx context.Context, database *mongo.Database, email string) (*User, error) {
	var err error

	err = database.Collection(userCollName).FindOne(ctx, bson.M{"user_email": email}).Decode(&obj)
	if err != nil {
		return nil, errors.New("No user exists with that email")
	}

	return obj, nil
}

// FindById
func (obj *User) FindById(ctx context.Context, database *mongo.Database) (*User, error) {
	var err error

	filter := bson.M{"_id": obj.ID}

	err = database.Collection(userCollName).FindOne(ctx, filter).Decode(&obj)
	if err != nil {
		return nil, errors.New("No user exists with that email")
	}

	return obj, nil
}

// SearchTutor
func (obj *User) SearchTutor(debugMode int, ctx context.Context, database *mongo.Database, keyword string) ([]User, error) {
	var err error
	var result []User

	keyword = ".*" + keyword + ".*"

	filter := bson.M{
		"$or": []bson.M{
			{"tutor_description": bson.M{"$regex": keyword, "$options": "i"}},
			{"user_email": bson.M{"$regex": keyword, "$options": "i"}},
			{"user_display_name": bson.M{"$regex": keyword, "$options": "i"}},
		},
		"is_tutor": true,
	}

	cursor, err := database.Collection(userCollName).Find(ctx, filter)
	if err != nil {
		logger.Println(debugMode, err.Error())
		return nil, errors.New("No assignment exists with that keyword in description")
	}

	if err = cursor.All(context.TODO(), &result); err != nil {
		logger.Println(debugMode, err.Error())
		return nil, err
	}

	for i := range result {
		result[i].Password = ""
	}

	return result, nil
}

// AddView
func (obj *User) AddView(debugMode int, ctx context.Context, database *mongo.Database, email string) (*User, error) {
	var err error

	filter := bson.M{"user_email": email}

	update := bson.D{
		{"$inc", bson.D{{"views", 1}}},
	}

	_, err = database.Collection(userCollName).UpdateOne(ctx, filter, update, options.Update().SetUpsert(true))
	if err != nil {
		return nil, err
	}

	obj.Password = ""

	return obj, nil
}
