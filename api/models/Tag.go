package models

import (
	"context"
	"errors"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Tag is struct to describe course objects
type Tag struct {
	ID        primitive.ObjectID `bson:"_id" json:"id" query:"id"`
	CreatedAt time.Time          `bson:"created_at" json:"created_at" query:"created_at"`
	UpdatedAt time.Time          `bson:"updated_at" json:"updated_at" query:"updated_at"`

	TagValue string `bson:"tag_value" json:"tag_value" form:"tag_value" query:"tag_value"`
	Views    int64  `bson:"views" json:"views" form:"views" query:"views"`
}

var tagCollName = "Tag"

// Validate performs minimal validation
func (obj *Tag) Validate() error {
	if len(obj.TagValue) < 2 {
		return errors.New("Tag value should be atleast 2 characters")
	}

	return nil
}

// Save saves the object in database
func (obj *Tag) Save(ctx context.Context, database *mongo.Database) (*Tag, error) {
	var err error
	// var result *mongo.InsertOneResult

	filter := bson.M{"tag_value": obj.TagValue}

	err = database.Collection(tagCollName).FindOne(ctx, filter).Decode(&obj)
	if err != nil {

		obj.ID = primitive.NewObjectID()
		obj.CreatedAt = time.Now()
		obj.UpdatedAt = time.Now()

		_, err = database.Collection(tagCollName).InsertOne(ctx, obj)
		if err != nil {
			return nil, err
		}

		return obj, nil
	}

	return nil, errors.New("User with that email already registered")
}

// AddView
func (obj *Tag) AddView(debugMode int, ctx context.Context, database *mongo.Database, tagValue string) (*Tag, error) {
	var err error

	filter := bson.M{"tag_value": tagValue}

	update := bson.D{
		{"$inc", bson.D{{"views", 1}}},
	}

	_, err = database.Collection(tagCollName).UpdateOne(ctx, filter, update, options.Update().SetUpsert(true))
	if err != nil {
		return nil, err
	}

	return obj, nil
}
