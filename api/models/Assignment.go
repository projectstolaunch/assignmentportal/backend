package models

import (
	"context"
	"errors"
	"log"
	"time"

	"gitlab.com/projectstolaunch/assignmentportal/backend/api/utils"
	"gitlab.com/projectstolaunch/assignmentportal/backend/logger"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Assignment is struct to describe course objects
type Assignment struct {
	ID        primitive.ObjectID `bson:"_id" json:"id" query:"id"`
	CreatedAt time.Time          `bson:"created_at" json:"created_at" query:"created_at"`
	UpdatedAt time.Time          `bson:"updated_at" json:"updated_at" query:"updated_at"`

	Title              string `bson:"title" json:"title" form:"title" query:"title"`
	DescriptionEnglish string `bson:"description_english" json:"description_english" form:"description_english" query:"description_english"`
	DescriptionFrench  string `bson:"description_french" json:"description_french" form:"description_french" query:"description_french"`

	DescriptionSolution string `bson:"description_solution" json:"description_solution" form:"description_solution" query:"description_solution"`

	Views      int64  `bson:"views" json:"views" form:"views" query:"views"`
	LinkFiles  string `bson:"link_files" json:"link_files" form:"link_files" query:"link_files"`
	Tags       string `bson:"tags" json:"tags" form:"tags" query:"tags"`
	UploadedBy string `bson:"uploaded_by" json:"uploaded_by" form:"uploaded_by" query:"uploaded_by"`

	IsVerified bool `bson:"is_verified" json:"is_verified" form:"is_verified" query:"is_verified"`
}

var assignmentCollName = "Assignment"

// Validate performs minimal validation
func (obj *Assignment) Validate() error {
	return nil
}

// Save saves the object in database
func (obj *Assignment) Save(ctx context.Context, database *mongo.Database) (*Assignment, error) {
	var err error
	// var result *mongo.InsertOneResult

	obj.Tags = utils.RemoveDuplicatesFromArray(obj.Tags)

	// result, err = database.Collection(assignmentCollName).InsertOne(ctx, obj)
	obj.ID = primitive.NewObjectID()
	obj.CreatedAt = time.Now()
	obj.UpdatedAt = time.Now()

	_, err = database.Collection(assignmentCollName).InsertOne(ctx, obj)
	if err != nil {
		return nil, err
	}

	return obj, nil
}

// Update by user_email
func (obj *Assignment) Update(ctx context.Context, database *mongo.Database) (*Assignment, error) {

	var err error

	filter := bson.M{"_id": obj.ID}

	obj.Tags = utils.RemoveDuplicatesFromArray(obj.Tags)

	update := bson.D{{"$set", bson.D{
		{"updated_at", time.Now()},
		{"title", obj.Title},
		{"description_english", obj.DescriptionEnglish},
		{"description_french", obj.DescriptionFrench},
		{"description_solution", obj.DescriptionSolution},
		{"link_files", obj.LinkFiles},
		{"tags", obj.Tags},
	}}}

	_, err = database.Collection(assignmentCollName).UpdateOne(ctx, filter, update)
	if err != nil {
		return nil, err
	}

	return obj, nil
}

// SearchByUploader by user_email
func (obj *Assignment) SearchByUploader(ctx context.Context, database *mongo.Database,
	userEmail string) ([]Assignment, error) {

	var err error
	var result []Assignment

	filter := bson.M{
		"uploaded_by": userEmail,
	}

	cursor, err := database.Collection(assignmentCollName).Find(ctx, filter)
	if err != nil {
		return nil, errors.New("No assignment exists with that keyword in description")
	}

	if err = cursor.All(context.TODO(), &result); err != nil {
		log.Fatal(err)
	}

	return result, nil
}

// Search by keyword
func (obj *Assignment) Search(debugMode int, ctx context.Context, database *mongo.Database, keyword string) ([]Assignment, error) {
	var err error
	var result []Assignment

	keyword = ".*" + keyword + ".*"

	filter := bson.M{
		"$or": []bson.M{
			{"description_english": bson.M{"$regex": keyword, "$options": "i"}},
			{"description_french": bson.M{"$regex": keyword, "$options": "i"}},
			{"description_solution": bson.M{"$regex": keyword, "$options": "i"}},
			{"tags": bson.M{"$regex": keyword, "$options": "i"}},
		},
	}

	logger.Println(debugMode, "Search: Querying DB for Search")
	cursor, err := database.Collection(assignmentCollName).Find(ctx, filter)
	if err != nil {
		logger.Println(debugMode, err.Error())
		return nil, errors.New("Search: No assignment exists with that keyword in description")
	}
	logger.Println(debugMode, "Search: Received response from DB")

	if err = cursor.All(context.TODO(), &result); err != nil {
		logger.Println(debugMode, err.Error())
		return nil, err
	}

	return result, nil
}

// FindById
func (obj *Assignment) FindById(debugMode int, ctx context.Context, database *mongo.Database, id string) (*Assignment, error) {
	var err error

	objId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		logger.Println(debugMode, "FindById: Failed to convert id to objectid")
		logger.Println(debugMode, err.Error())
		return nil, err
	}

	filter := bson.M{"_id": objId}

	err = database.Collection(assignmentCollName).FindOne(ctx, filter).Decode(obj)
	if err != nil {
		logger.Println(debugMode, "FindById: Failed to find object")
		logger.Println(debugMode, err.Error())
		return nil, err
	}

	return obj, nil
}

// AddView
func (obj *Assignment) AddView(debugMode int, ctx context.Context, database *mongo.Database, id string) (*Assignment, error) {
	var err error

	objId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		logger.Println(debugMode, "AddView: Failed to convert id to objectid")
		logger.Println(debugMode, err.Error())
		return nil, err
	}

	filter := bson.M{"_id": objId}

	update := bson.D{
		{"$inc", bson.D{{"views", 1}}},
	}

	_, err = database.Collection(assignmentCollName).UpdateOne(ctx, filter, update, options.Update().SetUpsert(true))
	if err != nil {
		return nil, err
	}

	return obj, nil
}
