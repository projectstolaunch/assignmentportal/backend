package models

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// Ticket is struct to describe course objects
type Ticket struct {
	ID        primitive.ObjectID `bson:"_id" json:"id" query:"id"`
	CreatedAt time.Time          `bson:"created_at" json:"created_at" query:"created_at"`
	UpdatedAt time.Time          `bson:"updated_at" json:"updated_at" query:"updated_at"`

	FullName    string `bson:"full_name" json:"full_name" form:"full_name" query:"full_name"`
	Subject     string `bson:"subject" json:"subject" form:"subject" query:"subject"`
	Description string `bson:"description" json:"description" form:"description" query:"description"`
	Email       string `bson:"email" json:"email" form:"email" query:"email"`
}

var ticketCollName = "Ticket"

// Validate performs minimal validation
func (obj *Ticket) Validate() error {
	return nil
}

// Save saves the object in database
func (obj *Ticket) Save(ctx context.Context, database *mongo.Database) (*Ticket, error) {
	var err error

	obj.ID = primitive.NewObjectID()
	obj.CreatedAt = time.Now()
	obj.UpdatedAt = time.Now()

	_, err = database.Collection(ticketCollName).InsertOne(ctx, obj)
	if err != nil {
		return nil, err
	}

	return obj, nil
}
