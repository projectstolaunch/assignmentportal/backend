package models

import (
	"context"
	"errors"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// SearchTerm is struct to describe course objects
type SearchTerm struct {
	ID        primitive.ObjectID `bson:"_id" json:"id" query:"id"`
	CreatedAt time.Time          `bson:"created_at" json:"created_at" query:"created_at"`
	UpdatedAt time.Time          `bson:"updated_at" json:"updated_at" query:"updated_at"`

	SearchTerm string `bson:"search_term" json:"search_term" form:"search_term" query:"search_term"`
	SearchType int    `bson:"search_type" json:"search_type" form:"search_type" query:"search_type"`
	Views      int64  `bson:"views" json:"views" form:"views" query:"views"`
}

var searchTermCollName = "SearchTerm"

// Validate performs minimal validation
func (obj *SearchTerm) Validate() error {
	if len(obj.SearchTerm) < 1 {
		return errors.New("SearchTerm should be atleast 1 characters")
	}

	return nil
}

// Save saves the object in database
func (obj *SearchTerm) Save(debugMode int, ctx context.Context, database *mongo.Database, searchValue string, searchType int) (*SearchTerm, error) {
	var err error

	filter := bson.M{"search_term": searchValue, "search_type": searchType}

	err = database.Collection(searchTermCollName).FindOne(ctx, filter).Decode(&obj)
	if err != nil {

		obj.ID = primitive.NewObjectID()
		obj.CreatedAt = time.Now()
		obj.UpdatedAt = time.Now()
		obj.SearchTerm = searchValue
		obj.SearchType = searchType
		obj.Views = 1

		_, err = database.Collection(searchTermCollName).InsertOne(ctx, obj)
		if err != nil {
			return nil, err
		}

		return obj, nil
	}

	update := bson.D{
		{"$inc", bson.D{{"views", 1}}},
	}

	_, err = database.Collection(searchTermCollName).UpdateOne(ctx, filter, update, options.Update().SetUpsert(true))
	if err != nil {
		return nil, err
	}

	return obj, nil
}
