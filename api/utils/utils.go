package utils

import (
	"encoding/base64"
	"fmt"
	"math/rand"
	"net/mail"
	"net/smtp"
	"strings"
)

var (
	lowerCharSet   = "abcdedfghijklmnopqrst"
	upperCharSet   = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	specialCharSet = "!@#$%&*"
	numberSet      = "0123456789"
	allCharSet     = lowerCharSet + upperCharSet + specialCharSet + numberSet
)

// RemoveDuplicatesFromArray removes duplicate elements from string array
func RemoveDuplicatesFromArray(tags string) string {

	var s = strings.Split(tags, ",")

	m := make(map[string]bool)
	for _, item := range s {
		if _, ok := m[item]; ok {
		} else {
			m[item] = true
		}
	}

	var result []string
	for item := range m {
		result = append(result, item)
	}

	resultTag := strings.Join(result, ",")
	return resultTag
}

// GeneratePassword generates random password
func GeneratePassword(passwordLength, minSpecialChar, minNum, minUpperCase int) string {
	var password strings.Builder

	//Set special character
	for i := 0; i < minSpecialChar; i++ {
		random := rand.Intn(len(specialCharSet))
		password.WriteString(string(specialCharSet[random]))
	}

	//Set numeric
	for i := 0; i < minNum; i++ {
		random := rand.Intn(len(numberSet))
		password.WriteString(string(numberSet[random]))
	}

	//Set uppercase
	for i := 0; i < minUpperCase; i++ {
		random := rand.Intn(len(upperCharSet))
		password.WriteString(string(upperCharSet[random]))
	}

	remainingLength := passwordLength - minSpecialChar - minNum - minUpperCase
	for i := 0; i < remainingLength; i++ {
		random := rand.Intn(len(allCharSet))
		password.WriteString(string(allCharSet[random]))
	}
	inRune := []rune(password.String())
	rand.Shuffle(len(inRune), func(i, j int) {
		inRune[i], inRune[j] = inRune[j], inRune[i]
	})
	return string(inRune)
}

func encodeRFC2047(String string) string {
	// use mail's rfc2047 to encode any string
	addr := mail.Address{Name: String, Address: ""}
	return strings.Trim(addr.String(), "")
}

// SendMail generates random password
func SendMail(targetEmail string, message string, subject string) error {
	// from := "support@coursehistory.com"
	// from := "vj1990.23@gmail.com"
	from := "coursehistory@aol.com"
	password := "gnrkqtisnowtatcq"

	// smtpHost := "mail.privateemail.com"
	// smtpHost := "smtp.gmail.com"
	smtpHost := "smtp.aol.com"
	smtpPort := "587"

	headerFrom := mail.Address{Name: "coursehistory", Address: from}
	headerTo := mail.Address{Name: targetEmail, Address: targetEmail}

	header := make(map[string]string)
	header["From"] = headerFrom.String()
	header["To"] = headerTo.String()
	header["Subject"] = subject
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = "text/plain; charset=\"utf-8\""
	header["Content-Transfer-Encoding"] = "base64"

	emailMessage := ""
	for k, v := range header {
		emailMessage += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	emailMessage += "\r\n" + base64.StdEncoding.EncodeToString([]byte(message))

	//messageBytes := []byte(message)
	auth := smtp.PlainAuth("", from, password, smtpHost)

	err := smtp.SendMail(smtpHost+":"+smtpPort, auth, headerFrom.Address, []string{headerTo.Address}, []byte(emailMessage))
	if err != nil {
		return err
	}

	return nil
}
