package controllers

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/projectstolaunch/assignmentportal/backend/api/models"
	"gitlab.com/projectstolaunch/assignmentportal/backend/logger"
)

// SearchAssignments is the route to home page
func (server *Server) SearchAssignments(c echo.Context) error {
	logger.Println(server.debugMode, "SearchAssignments: Serving Search Assignments")
	searchValue := c.QueryParam("searchValue")

	searchType := 1 // Assignment
	searchObj := new(models.SearchTerm)
	_, err := searchObj.Save(server.debugMode, server.GetContext(), server.GetDatabase(), searchValue, searchType)
	if err != nil {
		logger.Println(server.debugMode, "SearchAssignments: Failed to update SearchTerm Collection")
		logger.Println(server.debugMode, err.Error())
	}

	obj := new(models.Assignment)
	result, err := obj.Search(server.debugMode, server.GetContext(), server.GetDatabase(), searchValue)
	if err != nil {
		logger.Println(server.debugMode, "SearchAssignments: Failed to upload assignment")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	logger.Println(server.debugMode, "SearchAssignments: Served Search Assignments")
	return c.JSON(http.StatusOK, M{"solutions": result})
}

// SearchTutors is the route to home page
func (server *Server) SearchTutors(c echo.Context) error {
	searchValue := c.QueryParam("searchValue")

	searchType := 2 // Assignment
	searchObj := new(models.SearchTerm)
	_, err := searchObj.Save(server.debugMode, server.GetContext(), server.GetDatabase(), searchValue, searchType)
	if err != nil {
		logger.Println(server.debugMode, "SearchTutors: Failed to update SearchTerm Collection")
		logger.Println(server.debugMode, err.Error())
	}

	obj := new(models.User)
	result, err := obj.SearchTutor(server.debugMode, server.GetContext(), server.GetDatabase(), searchValue)
	if err != nil {
		logger.Println(server.debugMode, "SearchTutors: Failed to upload assignment")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	return c.JSON(http.StatusOK, M{"tutors": result})
}
