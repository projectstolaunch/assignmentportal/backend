package controllers

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/projectstolaunch/assignmentportal/backend/api/models"
	"gitlab.com/projectstolaunch/assignmentportal/backend/api/utils"
	"gitlab.com/projectstolaunch/assignmentportal/backend/logger"
)

// TicketSubmit is the route to home page
func (server *Server) TicketSubmit(c echo.Context) error {
	var err error

	obj := new(models.Ticket)

	logger.Println(server.debugMode, "TicketSubmit: Creating Ticket Object")
	if err = c.Bind(obj); err != nil {
		logger.Println(server.debugMode, "TicketSubmit: Failed to submit ticket")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	mailBody := obj.Description + "\n\n" + obj.FullName + "\n\n" + obj.Email
	err = utils.SendMail("coursehistory@aol.com", mailBody, obj.Subject)
	if err != nil {
		logger.Println(server.debugMode, "TicketSubmit: Failed to send email")
		logger.Println(server.debugMode, err.Error())
	}

	logger.Println(server.debugMode, "TicketSubmit: Saving Ticket Object")
	obj, err = obj.Save(server.GetContext(), server.GetDatabase())
	if err != nil {
		logger.Println(server.debugMode, "TicketSubmit: Failed to submit ticket")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	return c.JSON(http.StatusOK, obj)
}
