package controllers

import (
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"golang.org/x/crypto/bcrypt"

	stripe "github.com/stripe/stripe-go/v71"
	"github.com/stripe/stripe-go/v71/checkout/session"

	"gitlab.com/projectstolaunch/assignmentportal/backend/api/models"
	"gitlab.com/projectstolaunch/assignmentportal/backend/api/utils"
	"gitlab.com/projectstolaunch/assignmentportal/backend/logger"
)

var userNotFound = M{"message": "user not found"}

func verifyPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

// Register is the route to user registration
func (server *Server) Register(c echo.Context) error {
	var err error

	obj := new(models.User)
	if err = c.Bind(obj); err != nil {
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	if err = obj.Validate(); err != nil {
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	hashedPassword, err := hash(obj.Password)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}
	obj.Password = string(hashedPassword)

	obj, err = obj.Save(server.debugMode, server.GetContext(), server.GetDatabase())
	if err != nil {
		logger.Println(server.debugMode, "Register: Failed to register user")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	obj.Password = ""

	return c.JSON(http.StatusOK, obj)
}

// Login is a route for login
func (server *Server) Login(c echo.Context) error {
	var err error

	obj := new(models.User)
	if err = c.Bind(obj); err != nil {
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}
	password := obj.Password

	obj, err = obj.FindByEmail(server.GetContext(), server.GetDatabase(), obj.Email)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	err = verifyPassword(obj.Password, password)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, M{"message": "Incorrect Password. Please Try Again."})
	}

	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["name"] = obj.Email
	claims["admin"] = false
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix()
	t, err := token.SignedString([]byte("secret"))
	if err != nil {
		return c.JSON(http.StatusInternalServerError, M{"message": "Please Try Again."})
	}

	obj.Password = ""

	return c.JSON(http.StatusOK, M{"token": t, "user_info": obj})
}

// ForgotPassword
func (server *Server) ForgotPassword(c echo.Context) error {
	var err error

	obj := new(models.User)
	if err = c.Bind(obj); err != nil {
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	logger.Println(server.debugMode, "ForgotPassword: Finding User by Email")
	obj, err = obj.FindByEmail(server.GetContext(), server.GetDatabase(), obj.Email)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	logger.Println(server.debugMode, "ForgotPassword: Generating random Password")
	randomPass := utils.GeneratePassword(12, 1, 1, 1)
	hashedPassword, err := hash(randomPass)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}
	obj.Password = string(hashedPassword)
	obj.UpdatePassword(server.GetContext(), server.GetDatabase())

	obj.Password = ""

	logger.Println(server.debugMode, "ForgotPassword: Sending mail to user")
	message := "Your temporary password is - '" + randomPass + "'. Please change after logging in."
	subject := "Password Reset"
	err = utils.SendMail(obj.Email, message, subject)
	if err != nil {
		logger.Println(server.debugMode, "ForgotPassword: Failed to send the mail")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	obj.Password = ""

	logger.Println(server.debugMode, "ForgotPassword: Success, sending response")
	return c.JSON(http.StatusOK, M{"user_info": obj})
}

// ProfileUpdate is the route to user update
func (server *Server) ProfileUpdate(c echo.Context) error {
	var err error

	obj := new(models.User)
	if err = c.Bind(obj); err != nil {
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	obj, err = obj.Update(server.GetContext(), server.GetDatabase())
	if err != nil {
		logger.Println(server.debugMode, "ProfileUpdate: Failed to update user")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	obj.Password = ""

	return c.JSON(http.StatusOK, obj)
}

// ProfileUpdateToTutor
func (server *Server) ProfileUpdateToTutor(c echo.Context) error {
	var err error

	obj := new(models.User)
	if err = c.Bind(obj); err != nil {
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	obj, err = obj.FindByEmail(server.GetContext(), server.GetDatabase(), obj.Email)
	if err != nil {
		logger.Println(1, "ProfileUpdateToTutorPayment: Failed to upgrade to tutor. User not found")
		logger.Println(1, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	logger.Println(server.debugMode, obj.Email)

	// obj, err = obj.Update(server.GetContext(), server.GetDatabase())
	// if err != nil {
	// 	logger.Println(server.debugMode, "ProfileUpdate: Failed to update user")
	// 	logger.Println(server.debugMode, err.Error())
	// 	return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	// }

	stripe.Key = "pk_live_51I2k7BIBElZPpdPwLGbMPyHpNEDxMI3ZtZnsU8KZZw0j2SsCdzu1ESCf11eHwzB92rUzBprUPz1PqFMhQXhGL34r00JfgK7goq"

	params := &stripe.CheckoutSessionParams{
		PaymentMethodTypes: stripe.StringSlice([]string{
			"card",
		}),

		LineItems: []*stripe.CheckoutSessionLineItemParams{
			&stripe.CheckoutSessionLineItemParams{
				PriceData: &stripe.CheckoutSessionLineItemPriceDataParams{
					Currency: stripe.String(string(stripe.CurrencyCAD)),
					ProductData: &stripe.CheckoutSessionLineItemPriceDataProductDataParams{
						Name: stripe.String("Upgrade to Tutor"),
					},
					// UnitAmount: stripe.Int64(2000),
					UnitAmountDecimal: stripe.Float64(14.99 * 100),
				},
				Quantity: stripe.Int64(1),
			},
		},
		Mode:       stripe.String(string(stripe.CheckoutSessionModePayment)),
		SuccessURL: stripe.String("http://localhost:4000/profile/updatetotutorpayment" + "?success=true&email=" + obj.Email),
		CancelURL:  stripe.String("http://localhost:4000/profile/updatetotutorpayment" + "?success=false&email=" + obj.Email),
	}

	session, err := session.New(params)
	if err != nil {
		logger.Println(server.debugMode, "ProfileUpdateToTutor: Failed to proceed with stripe payment")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	// return c.JSON(http.StatusOK, obj)
	return c.JSON(http.StatusOK, M{"SessionID": session.ID})
}

// ProfileUpdateToTutorPayment
func (server *Server) ProfileUpdateToTutorPayment(c echo.Context) error {
	var err error

	success := c.QueryParam("success")
	email := c.QueryParam("email")

	if success == "true" {
		logger.Println(1, "true")
		// return c.JSON(http.StatusOK, M{"success": true})
		obj := new(models.User)
		obj, err = obj.FindByEmail(server.GetContext(), server.GetDatabase(), email)
		if err != nil {
			logger.Println(1, "ProfileUpdateToTutorPayment: Failed to upgrade to tutor. Payment was success")
			logger.Println(1, err.Error())
			return c.Redirect(http.StatusMovedPermanently, "http://localhost:3000/profile")
		}

		obj, err = obj.UpgradeToTutor(server.GetContext(), server.GetDatabase())
		if err != nil {
			logger.Println(1, "ProfileUpdateToTutorPayment: Failed to upgrade to tutor. Payment was success")
			logger.Println(1, err.Error())
			return c.Redirect(http.StatusMovedPermanently, "http://localhost:3000/profile")
		}

		return c.Redirect(http.StatusMovedPermanently, "http://localhost:3000/profile")
	}
	logger.Println(1, "false")
	return c.Redirect(http.StatusMovedPermanently, "http://localhost:3000/profile")
}

// GetProfile is the route to user update
func (server *Server) GetProfile(c echo.Context) error {
	var err error

	obj := new(models.User)
	userEmail := c.QueryParam("user_email")

	logger.Println(server.debugMode, "GetProfile: Fetching details for "+userEmail)
	obj, err = obj.FindByEmail(server.GetContext(), server.GetDatabase(), userEmail)
	if err != nil {
		logger.Println(server.debugMode, "GetProfile: Failed to fetch user")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	logger.Println(server.debugMode, "GetProfile: Fetching User Assignments")
	assignmentObj := new(models.Assignment)

	result, err := assignmentObj.SearchByUploader(server.GetContext(), server.GetDatabase(), obj.Email)
	if err != nil {
		logger.Println(server.debugMode, "GetProfile: Failed to fetch user assignments")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": "Please Try Again."})
	}

	obj.Password = ""

	logger.Println(server.debugMode, "Sending response to server")
	return c.JSON(http.StatusOK, M{"user_info": obj, "solution_uploads": result})
}

// AddView AddView
func (server *Server) AddUserView(c echo.Context) error {
	var err error

	obj := new(models.User)
	userEmail := c.QueryParam("user_email")

	logger.Println(server.debugMode, "AddUserView: Fetching details for "+userEmail)
	obj, err = obj.AddView(server.debugMode, server.GetContext(), server.GetDatabase(), userEmail)
	if err != nil {
		logger.Println(server.debugMode, "AddUserView: Failed to add user view")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	obj.Password = ""

	logger.Println(server.debugMode, "Sending response to server")
	return c.JSON(http.StatusOK, M{"result": true})
}

func (server *Server) AddAdminUser() {
	var err error

	obj := new(models.User)
	obj.Nickname = "vj1990"
	obj.Email = "vj1990.23@gmail.com"
	obj.Phone = "4389248180"
	obj.Password = "temppass"
	obj.IsAdmin = true
	obj.IsTutor = true
	obj.IsVerified = true

	if err = obj.Validate(); err != nil {
		logger.Println(server.debugMode, "AddAdminUser: Failed to create admin user")
		logger.Println(server.debugMode, err.Error())
		return
	}

	hashedPassword, err := hash(obj.Password)
	if err != nil {
		logger.Println(server.debugMode, "AddAdminUser: Failed to create admin user")
		logger.Println(server.debugMode, err.Error())
		return
	}
	obj.Password = string(hashedPassword)

	obj, err = obj.SaveAdmin(server.debugMode, server.GetContext(), server.GetDatabase())
	if err != nil {
		logger.Println(server.debugMode, "AddAdminUser: Failed to create admin user")
		logger.Println(server.debugMode, err.Error())
		return
	}

	return
}
