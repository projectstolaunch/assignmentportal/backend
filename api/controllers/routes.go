package controllers

func (server *Server) initializeRoutes() {
	server.echoObj.GET("/", server.Home)

	server.echoObj.POST("register", server.Register)
	server.echoObj.POST("login", server.Login)
	server.echoObj.POST("forgotpassword", server.ForgotPassword)

	server.echoObj.POST("profile/update", server.ProfileUpdate)
	server.echoObj.GET("profile/fetch", server.GetProfile)
	server.echoObj.POST("profile/updatetotutor", server.ProfileUpdateToTutor)
	server.echoObj.GET("profile/updatetotutorpayment", server.ProfileUpdateToTutorPayment)

	// Add User View
	server.echoObj.GET("profile/addview", server.AddUserView)

	server.echoObj.POST("assignment/upload", server.AssignmentUpload)
	server.echoObj.GET("assignment/user/uploads", server.AssignmentUserUploads)
	server.echoObj.POST("assignment/update", server.AssignmentUpdate)
	// Find assignment by id
	server.echoObj.GET("assignment/fetch", server.AssignmentFetch)
	// Add Assignment View
	server.echoObj.GET("assignment/addview", server.AddAssignmentView)

	// Add Tag View
	server.echoObj.GET("tag/addview", server.AddTagView)

	server.echoObj.GET("search/assignments", server.SearchAssignments)
	server.echoObj.GET("search/tutors", server.SearchTutors)

	// Ticket Submit
	server.echoObj.POST("ticket/submit", server.TicketSubmit)
}
