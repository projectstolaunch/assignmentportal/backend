package controllers

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
	"github.com/labstack/echo"
	"gitlab.com/projectstolaunch/assignmentportal/backend/api/models"
	"gitlab.com/projectstolaunch/assignmentportal/backend/logger"
)

// AssignmentUpload is the route to home page
func (server *Server) AssignmentUpload(c echo.Context) error {
	var err error

	obj := new(models.Assignment)
	if err = c.Bind(obj); err != nil {
		logger.Println(server.debugMode, "AssignmentUpload: Failed to extract from POST")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	logger.Println(server.debugMode, "AssignmentUpload: Validating data")
	if err = obj.Validate(); err != nil {
		logger.Println(server.debugMode, "AssignmentUpload: Failed to Validate")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	obj, err = obj.Save(server.GetContext(), server.GetDatabase())
	if err != nil {
		logger.Println(server.debugMode, "AssignmentUpload: Failed to upload assignment")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	// Post to Socials
	message := "A new assignment has been uploaded. Title = " + obj.Title + ". Tags = " + obj.Tags
	linkToAssignment := os.Getenv("DOMAIN_NAME") + "/pub/upload?id=" + obj.ID.Hex()
	err = server.postToSocialMediaFacebook(message, linkToAssignment)
	if err != nil {
		logger.Println(server.debugMode, "AssignmentUpload: Failed to post to social media facebook")
		logger.Println(server.debugMode, err.Error())
	}

	// Port to Twitter
	messageTweet := "A new assignment has been uploaded. Title = " + obj.Title + ". "
	tmpTagsTweet := strings.Split(obj.Tags, ",")
	numTags := len(tmpTagsTweet)

	for i := 0; i < numTags; i++ {
		messageTweet = messageTweet + " #" + tmpTagsTweet[i]
	}

	err = server.postToSocialMediaTwitter(messageTweet, linkToAssignment)
	if err != nil {
		logger.Println(server.debugMode, "AssignmentUpload: Failed to post to social media Twitter")
		logger.Println(server.debugMode, err.Error())
	}

	// Save Tags
	objTag := new(models.Tag)
	tmpTags := strings.Split(obj.Tags, ",")

	for _, element := range tmpTags {
		objTag.TagValue = element
		_, err = objTag.Save(server.GetContext(), server.GetDatabase())
		if err != nil {
			logger.Println(server.debugMode, "AssignmentUpload: Failed to save tag, but Assignment Saved")
			logger.Println(server.debugMode, err.Error())
		}
	}

	return c.JSON(http.StatusOK, obj)
}

// AssignmentUserUploads gets user assignments
func (server *Server) AssignmentUserUploads(c echo.Context) error {
	var err error

	userEmail := c.QueryParam("user_email")
	assignmentObj := new(models.Assignment)

	result, err := assignmentObj.SearchByUploader(server.GetContext(), server.GetDatabase(), userEmail)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, M{"message": "Please Try Again."})
	}

	return c.JSON(http.StatusOK, M{"solution_uploads": result})
}

// AssignmentUpdate updates the assignment
func (server *Server) AssignmentUpdate(c echo.Context) error {
	var err error

	obj := new(models.Assignment)
	if err = c.Bind(obj); err != nil {
		logger.Println(server.debugMode, "AssignmentUpdate: Failed to extract from POST")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	logger.Println(server.debugMode, "AssignmentUpdate: Validating data")
	if err = obj.Validate(); err != nil {
		logger.Println(server.debugMode, "AssignmentUpdate: Failed to Validate")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	obj, err = obj.Update(server.GetContext(), server.GetDatabase())
	if err != nil {
		logger.Println(server.debugMode, "AssignmentUpdate: Failed to update assignment")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	// Post to Socials
	message := "A new assignment has been uploaded. Title = " + obj.Title + ". Tags = " + obj.Tags
	linkToAssignment := os.Getenv("DOMAIN_NAME") + "/pub/upload?id=" + obj.ID.Hex()
	err = server.postToSocialMediaFacebook(message, linkToAssignment)
	if err != nil {
		logger.Println(server.debugMode, "AssignmentUpload: Failed to post to social media facebook")
		logger.Println(server.debugMode, err.Error())
	}

	// Port to Twitter
	messageTweet := "A new assignment has been uploaded. Title = " + obj.Title + ". "
	tmpTagsTweet := strings.Split(obj.Tags, ",")
	numTags := len(tmpTagsTweet)

	for i := 0; i < numTags; i++ {
		messageTweet = messageTweet + " #" + tmpTagsTweet[i]
	}

	err = server.postToSocialMediaTwitter(messageTweet, linkToAssignment)
	if err != nil {
		logger.Println(server.debugMode, "AssignmentUpload: Failed to post to social media Twitter")
		logger.Println(server.debugMode, err.Error())
	}

	// Save Tags
	objTag := new(models.Tag)
	tmpTags := strings.Split(obj.Tags, ",")

	for _, element := range tmpTags {
		objTag.TagValue = element
		_, err = objTag.Save(server.GetContext(), server.GetDatabase())
		if err != nil {
			logger.Println(server.debugMode, "AssignmentUpdate: Failed to save tag, but Assignment Updated")
			logger.Println(server.debugMode, err.Error())
		}
	}

	return c.JSON(http.StatusOK, obj)
}

// AssignmentFetch
func (server *Server) AssignmentFetch(c echo.Context) error {
	var err error

	assignmentId := c.QueryParam("id")
	assignmentObj := new(models.Assignment)

	result, err := assignmentObj.FindById(server.debugMode, server.GetContext(), server.GetDatabase(), assignmentId)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, M{"message": "Please Try Again."})
	}

	return c.JSON(http.StatusOK, M{"data": result})
}

func (server *Server) postToSocialMediaFacebook(message, linkToAssignment string) error {
	fbToken := os.Getenv("FB_PAGE_TOKEN")

	URL := "https://graph.facebook.com/"
	pageId := "105499841469997/"
	// targetURL := URL + pageId + "feed?message=" + message + "&link=" + linkToAssignment + "&access_token=" + fbToken
	targetURL := URL + pageId + "feed"

	logger.Println(server.debugMode, targetURL)

	requestBody, err := json.Marshal(map[string]string{
		"message":      message + ". " + linkToAssignment,
		"link":         linkToAssignment,
		"access_token": fbToken,
	})
	if err != nil {
		logger.Println(server.debugMode, "postToSocialMedias: Failed to marshal post body")
		logger.Println(server.debugMode, err.Error())
		return err
	}

	resp, err := http.Post(targetURL, "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		logger.Println(server.debugMode, "postToSocialMedias: Failed to POST")
		logger.Println(server.debugMode, err.Error())
		return err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logger.Println(server.debugMode, "postToSocialMedias: Failed to read response")
		logger.Println(server.debugMode, err.Error())
		return err
	}

	logger.Println(server.debugMode, string(body))

	return nil
}

func (server *Server) postToSocialMediaTwitter(message, linkToAssignment string) error {
	config := oauth1.NewConfig("nG7E5zwIqcUAAyw253zMZMR3a", "2OMmy4S0brf2eXZsviC5MutXNYT2RIs9CSooFNcQ4ZvQaEA828")
	token := oauth1.NewToken("1342613860386283521-2gPMUwlFrZ1h3vJadniBYnNdbFE6cc", "5wVUeZI2dfBanzMIl70qJLbPF73LOKGwvRQd35g9EapUW")
	httpClient := config.Client(oauth1.NoContext, token)

	// Twitter client
	client := twitter.NewClient(httpClient)

	// tweet, resp, err := client.Statuses.Update("just setting up my twttr", nil)
	_, _, err := client.Statuses.Update(message+". Visit "+linkToAssignment, nil)
	if err != nil {
		logger.Println(server.debugMode, "postToSocialMediaTwitter: Failed to tweet")
		logger.Println(server.debugMode, err.Error())
		return err
	}
	logger.Println(server.debugMode, "postToSocialMediaTwitter: Tweet Success")
	return nil
}

// AddView AddView
func (server *Server) AddAssignmentView(c echo.Context) error {
	var err error

	obj := new(models.Assignment)
	id := c.QueryParam("id")

	logger.Println(server.debugMode, "AddAssignmentView: Fetching details for assignment: "+id)
	obj, err = obj.AddView(server.debugMode, server.GetContext(), server.GetDatabase(), id)
	if err != nil {
		logger.Println(server.debugMode, "AddAssignmentView: Failed to add assignment view")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	logger.Println(server.debugMode, "Sending response to server")
	return c.JSON(http.StatusOK, M{"result": true})
}
