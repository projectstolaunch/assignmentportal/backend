package controllers

import (
	"net/http"

	"github.com/labstack/echo"

	"gitlab.com/projectstolaunch/assignmentportal/backend/api/models"
	"gitlab.com/projectstolaunch/assignmentportal/backend/logger"
)

var tagNotFound = M{"message": "tag not found"}

// AddView AddView
func (server *Server) AddTagView(c echo.Context) error {
	var err error

	obj := new(models.Tag)
	tagValue := c.QueryParam("tag_value")

	logger.Println(server.debugMode, "AddTagView: Fetching details for Tag: "+tagValue)
	obj, err = obj.AddView(server.debugMode, server.GetContext(), server.GetDatabase(), tagValue)
	if err != nil {
		logger.Println(server.debugMode, "AddTagView: Failed to add user view")
		logger.Println(server.debugMode, err.Error())
		return c.JSON(http.StatusInternalServerError, M{"message": err.Error()})
	}

	logger.Println(server.debugMode, "Sending response to server")
	return c.JSON(http.StatusOK, M{"result": true})
}
