package controllers

import (
	"net/http"

	"github.com/labstack/echo"
)

// Home is the route to home page
func (server *Server) Home(c echo.Context) error {
	return c.JSON(http.StatusOK, "Welcome to the Student Portal")
}
