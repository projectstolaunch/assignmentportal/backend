package controllers

import (
	"context"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"gitlab.com/projectstolaunch/assignmentportal/backend/logger"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Server is the struct for the server
type Server struct {
	echoObj   *echo.Echo
	database  *mongo.Database
	ctx       context.Context
	debugMode int
}

// M interface for errors
type M map[string]interface{}

// InternalError is server internal error message
var InternalError = M{"message": "internal error"}

// Initialize initializes the server
func (server *Server) Initialize(DebugMode int, Dbdriver, DbUser, DbPassword, DbPort, DbHost, DbName string, ConnTimeout int) {
	server.ctx = context.TODO()
	server.debugMode = DebugMode

	if Dbdriver == "mongo" {
		var mongoURI string

		if len(DbUser) > 0 {
			mongoURI = "mongodb://" + DbUser + ":" + DbPassword + "@" + DbHost + DbPort
		} else {
			mongoURI = "mongodb://" + DbHost + DbPort
		}

		logger.Println(server.debugMode, mongoURI)
		clientOptions := options.Client().ApplyURI(mongoURI)
		client, err := mongo.Connect(server.ctx, clientOptions)

		if err != nil {
			logger.Println(server.debugMode, err.Error())
			logger.Fatal(server.debugMode, "Could not initialize database connection")
		}

		err = client.Ping(server.ctx, nil)
		if err != nil {
			logger.Println(server.debugMode, err.Error())
			logger.Fatal(server.debugMode, "Could not initialize database connection")
		}

		server.database = client.Database(DbName)
	}

	server.echoObj = echo.New()
	server.echoObj.Use(middleware.CORS())

	server.initializeRoutes()
	server.AddAdminUser()
}

// GetDatabase gets the database server is connected to
func (server *Server) GetDatabase() *mongo.Database {
	return server.database
}

// GetContext gets the server context
func (server *Server) GetContext() context.Context {
	return server.ctx
}

// Run is the entrypoint for the server to start
func (server *Server) Run(addr string) {
	logger.Println(server.debugMode, "Listening to port 4000")
	server.echoObj.Logger.Fatal(server.echoObj.Start("0.0.0.0:4000"))
}
