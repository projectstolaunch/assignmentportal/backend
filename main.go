package main

import (
	"gitlab.com/projectstolaunch/assignmentportal/backend/api"
)

func main() {
	api.Run()
}
