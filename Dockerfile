FROM golang:1.13-alpine3.11 as builder

#USER root
RUN mkdir /app
ADD . /app
WORKDIR /app

RUN apk update && \
    apk upgrade && \
    apk add git

COPY go.mod .
COPY go.sum .
RUN go mod download

#RUN go build -o main .
RUN CGO_ENABLED=0 GOOS=linux go build -a -o main .

#USER 1001
#CMD ["/app/main"]




FROM alpine:3.11.3
COPY --from=builder /app/main .
COPY --from=builder /app/.env .

# executable
ENTRYPOINT [ "./main" ]
# arguments that can be overridden
#CMD [ "3", "300" ]